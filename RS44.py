from random import *
import shlex

s = 0
debug = True

grid_dim = 25
fill_size = 15
fill_var = 3

def getgridline():
    global grid_dim, fill_size, fill_var
    line = ['_' for i in range(grid_dim)]
    fill = fill_size + randint(-fill_var, fill_var)
    vac = grid_dim - fill
    for i in range(fill):
        line[i] = '#'
    shuffle(line)
    return (line, vac)

def getgrid():
    global grid_dim
    grid = []
    size = 0
    for i in range(grid_dim):
        line, vac = getgridline()
        size += vac
        grid.extend(line)
    return (grid, size)

def getperm():
    global grid_dim
    perm = [i for i in range(grid_dim)]
    shuffle(perm)
    return perm

def getsheet():
    global s
    seed(s)
    perm = getperm()
    grid, size = getgrid()
    return (perm, grid, size)

def addmsg(sheet, start, msg):
    global grid_dim
    if not start:
        return sheet
    perm, grid, size = sheet
    if len(msg) > size:
        return sheet
    mi = 0
    for i in range(grid_dim * grid_dim):
        ind = (i + start) % (grid_dim * grid_dim)
        if grid[ind] == '_':
            grid[ind] = msg[mi]
            mi += 1
            if mi >= len(msg):
                break
    return (perm, grid, size)

def getmsg(sheet):
    global grid_dim
    perm, grid, size = sheet
    ss = []
    for i in range(grid_dim):
        for col in range(grid_dim):
            if perm[col] == i:
                break
        for j in range(grid_dim):
            ind = j * grid_dim + col
            if not grid[ind] in "#_":
                ss.append(grid[ind])
    return "-".join(ss)

##### Display Functions

def getpermstr(perm):
    global grid_dim
    w = len(str(grid_dim * grid_dim))
    ss = ""
    for i in range(grid_dim):
        ss += str(perm[i]).ljust(w)
    ss += '|' + '\n'
    return ss

def getgridstr(grid):
    global grid_dim
    w = len(str(grid_dim * grid_dim))
    ss = ""
    for i in range(grid_dim * w):
        ss += '-'
    ss += '+' + '\n'
    for i in range(grid_dim):
        for j in range(grid_dim):
            ss += str(grid[i * grid_dim + j]).ljust(w)
        ss += '|' + str(i) + '\n'
    for i in range(grid_dim * w):
        ss += '-'
    ss += '+' + '\n'
    return ss

def getsheetstr(sheet):
    global grid_dim
    w = len(str(grid_dim * grid_dim))
    perm, grid, size = sheet
    ss = "Max message size: " + str(size) + '\n'
    for i in range(grid_dim * w):
        ss += '-'
    ss += '+' + '\n'
    ss += getpermstr(perm)
    for i in range(grid_dim * w):
        ss += '-'
    ss += '+' + '\n'
    ss += getpermstr([i for i in range(grid_dim)])
    ss += getgridstr(grid)
    return ss

##### Loop Functions

def quitf(argv):
    if not len(argv) == 1:
        print "USAGE: quit"
        return
    exit(0)

def setseed(argv):
    global s
    if not len(argv) == 2:
        print "USAGE: seed <new seed>"
        return
    try:
        ns = int(argv[1])
    except:
        ns = hash(argv[1])
    print "NEW SEED: " + str(ns)
    s = ns

def showsheet(argv):
    global grid_dim
    if not (len(argv) == 1 or len(argv) == 3 or len(argv) == 4):
        print "USAGE: sheet [<start> <length> [<show>]]"
        print "USAGE: sheet [<start> <message> [<show>]]"
        return
    start = None
    msg = None
    if len(argv) >= 3:
        try:
            start = int(argv[1])
        except:
            try:
                start = argv[1].split(",")
                if len(start) > 2:
                    start = None
                start = int(start[0]) * grid_dim + int(start[1])
            except:
                print "<start> format: <cell number>"
                print "<start> format: <row>,<column>"
                return
        try:
            msg = int(argv[2])
            msg = [str(i + 1) for i in range(msg)]
        except:
            msg = [str(i).upper() for i in argv[2] if i.isalpha()]
    sheet = getsheet()
    if start:
        print "start: cell " + str(start) + " (row " + str(start / grid_dim) + ", column " + str(start % grid_dim) + ")"
        print "msg: " + "-".join(msg)
    sheet = addmsg(sheet, start, msg)
    ss = getsheetstr(sheet)
    if len(argv) == 4:
        try:
            show = int(argv[3])
            if not show == 0:
                ss += "encoded msg: " + getmsg(sheet) + '\n'
        except:
            print "<show> format: <int>"
    print ss,
    
def main():
    global debug
    while True:
        inp = raw_input("> ")
        argv = shlex.split(inp)
        if debug:
            print argv
        try:
            cmd = argv[0]
        except:
            cmd = None
        if not cmd:
            pass
        elif cmd == 'quit' or cmd == 'q':
            quitf(argv)
        elif cmd == "seed":
            setseed(argv)
        elif cmd == "sheet":
            showsheet(argv)
        else:
            print "UNKNOWN: " + cmd

if __name__ == '__main__':
    main()
